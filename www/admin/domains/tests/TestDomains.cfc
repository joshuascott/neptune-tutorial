<cfcomponent displayname="Domains" extends="com.sebtools.RecordsTester" output="false">

<cffunction name="beforeTests" access="public" returntype="void" output="no">
	
	<cfset loadExternalVars("Domains")>
	
</cffunction>

<cffunction name="shouldReturnPathOnly" access="public" returntype="void" output="no"
	hint="This URL IS found in the array of domains and the function should return just the path."
	mxunit:transaction="rollback"
>
	<cfset var DomainID = saveDomain(DomainName="cnn.com") />
	<cfset var url = 'http://www.cnn.com/home/index.html'>

	<cfset assertEquals(Variables.Domains.convertURL(url),"/home/index.html","The function should have returned only the path.")>

</cffunction>

<cffunction name="shouldReturnWholeDomain" access="public" returntype="void" output="no"
	hint="The URL IS NOT found in the array of domains and therefore should return the URL passed in back out."
	mxunit:transaction="rollback"
>

	<cfset var url = 'http://www.abc.com/home/index.html'>

	<cfset assertEquals(Variables.Domains.convertURL(url),url,"The function should have passed back the url passed in.")>

</cffunction>

</cfcomponent>