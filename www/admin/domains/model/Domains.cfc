<cfcomponent displayname="Domains" extends="com.sebtools.Records" output="false">

<cffunction name="convertURL" access="public" returntype="string" output="no">
	<cfargument name="URL" type="string" required="true" default="">

	<cfset var convertedURL = ''>
	<cfset var javaUrl = createObject( "java", "java.net.URL" ).init(javaCast( "string", arguments.url )) >
	<cfset var path = javaUrl.getPath()>
	<cfset var wwwdomain = javaUrl.getHost()>
	<cfset var justdomain = ''>

	<!--- get domain without www --->
	<cfset justdomain = mid(wwwdomain,5,len(wwwdomain))>

	<!--- check to see if it is in the sample table --->
	<cfif hasDomain(DomainName=justdomain)>
		<cfset convertedURL = path>
	<cfelse>
		<cfset convertedURL = arguments.URL>
	</cfif>

	<cfreturn trim(convertedURL)>
</cffunction>

<cffunction name="xml" access="public" output="yes">
<table entity="Domain"></table>
</cffunction>

</cfcomponent>